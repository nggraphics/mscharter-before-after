var gulp = require('gulp');
var browserSync = require('browser-sync');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('js', function () {
    gulp.src('*.js', {cwd: 'js'})
    	.pipe(uglify())
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./'))
        .pipe(browserSync.reload({stream:true}));
});