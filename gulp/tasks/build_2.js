var browserSync    = require('browser-sync');
var concat         = require('gulp-concat');
var csso           = require('gulp-csso');
var fileinclude    = require('gulp-file-include');
var gulp           = require('gulp');
var gulpif         = require('gulp-if');
var inquirer       = require('inquirer');
var rename         = require('gulp-rename');
var rimraf         = require('gulp-rimraf');
var runSequence    = require('run-sequence');
var sass 		   = require('gulp-sass');
var smoosher       = require('gulp-smoosher');
var template       = require('gulp-template-compile');
var uglify         = require('gulp-uglify');
var useref         = require('gulp-useref');


var COMPILE;

function getDirectories(baseDir) {
	return fs.readdirSync(baseDir).filter(function (file) {
		return fs.statSync(baseDir + '/' + file).isDirectory();
	});
}


gulp.task('clean', function() {
	return gulp.src(['.tmp','COMPILED.html'], {
		read: false,
		cwd: './'
	}).pipe(rimraf());
});

gulp.task('compile-stylesheets', function() {
	return gulp.src('css/*', {cwd: './'})
		.pipe(sass({compass: true}))
		.pipe(gulp.dest('.tmp', {cwd: './'}))
		.pipe(browserSync.reload({stream:true}));
});

gulp.task('build-html', function() {

	return gulp.src('/template' + GRAPHIC_TEMPLATE + '.html')
		.pipe(fileinclude())
		.pipe(rename('index.html'))
		.pipe(gulp.dest('./'))
		.pipe(browserSync.reload({stream:true}));
});

gulp.task('compile-templates', function() {
	return gulp.src([
			// 'common/js/templates/*.template',
			// 'graphics/' + GRAPHIC + '/js/templates/*.template'
		])
		.pipe(template({
			templateSettings: {
				interpolate: /{{([\s\S]+?)}}/g,
				evaluate:    /{=([\s\S]+?)=}/g
			}
		}))
		// .pipe(concat('templates.js'))
		.pipe(gulp.dest('.tmp', {cwd: './'}))
		.pipe(browserSync.reload({stream:true}));
});

gulp.task('build-html-prod', function() {
	return gulp.src('template-prod.html', {cwd: './'})
		.pipe(fileinclude())
		.pipe(rename('COMPILED.html'))
		.pipe(gulp.dest('./', {cwd: './'}));
});

gulp.task('minify', function() {

	var assets = useref.assets();

	return gulp.src('COMPILED.html', {cwd: './'})
		.pipe(assets)
		.pipe(gulpif('*.js', uglify()))
		.pipe(gulpif('*.css', csso(true)))
		.pipe(assets.restore())
		.pipe(useref())
		.pipe(gulp.dest('./', {cwd: './'}));
});

gulp.task('smoosher', function() {

	return gulp.src('COMPILED.html', {cwd: './'})
		.pipe(smoosher({
			cssTags: {
				begin: '<style>',
				end: '</style>'
			}
		}))
		.pipe(gulp.dest('COMPILED.html', {cwd: './'}));
});

gulp.task('build', function(done) {

	var prompts = [{
		type: 'confirm',
		name: 'compile',
		message: 'Do you want to compile?',
		default: false
	}];

	inquirer.prompt(prompts, function(answers) {

		COMPILE = answers.compile;
		GRAPHIC_TEMPLATE = '';

		if (COMPILE) {

			runSequence(
				'clean'
				,'compile-stylesheets'
				,'minify'
				,'smoosher'
				,'build-html-prod'
			);

		} else {
			runSequence(
			'watch',
			'html',
			'browser-sync',
			'sass'
			);
			
		}
	});
})


gulp.task('default', ['build'], function() {

	build();

});
